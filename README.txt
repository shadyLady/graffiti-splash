How to use Gitlab without breaking shit. 

Klaarmaken van je werkomgeving

Als eerst moet je een map klaarzetten waarin je het project wilt importeren. Wanneer je dit gedaan hebt, klik je met de rechtermuisknop op de map en 
kies je Git Bash here. Er opent dan een command prompt. In dit prompt wil je als eerst invoeren "git init" (zonder quotes). Dit zorgt ervoor dat je map
klaargemaakt wordt om met git te kunnen werken, indien je hidden files zichtbaar hebt staan verschijnt er in de map een .git folder. 

Aanmaken SSH key 

Om je map te kunnen synchen met het project op gitlab heb je een SSH key nodig, een soort van beveiligings sleutel. Als je in Gitlab op Profile Settings 
klikt zie je in je menu een tabje "SSH Keys". Als je daarop klikt, zie je in de rechterbovenhoek een groene knop " + Add SSH key". Als je hierop klikt 
krijg je een venster waarin je de key in moet voeren en een venster waarin je een titel voor de key in kunt voeren. 

Open je Git Bash prompt en voer in : 

ssh-keygen -t rsa "je@emailadres.com" 

Ik bedoel hiermee dus het emailadres waarmee je je aangemeld hebt op gitlab! 

Na het invoeren van deze command wordt je gevraagd een locatie en filename in te voeren. Gewoon op Enter drukken, dan wordt de default locatie gekozen. 
Ook wordt je gevraagd een wachtwoord in te voeren. Wederom is dit niet verplicht, mocht je dit wel willen doen moet het wachtwoord bestaan uit minstens 
vier tekens. Pas goed op wat je kiest, volgens mij is er geen manier je wachtwoord gemakkelijk te achterhalen indien je deze vergeten bent. 

Als je key correct aangemaakt is verschijnt er een hele hoop tekst en een ascii artwerkje. Nadat dit gebeurt is kun je de key kopieren door hetvolgende 
commando te geven: 

cat ~/.ssh/id_rsa.pub

Plak de gekopieerde code in het keyvenster in gitlab en geef de key een naam. De naam maakt niets uit, vrije keus. 

Nadat je de key opgeslagen hebt zou je in staat moeten zijn het project uit gitlab naar je lokale map te pullen. Dit doe je als volgt 

git pull git@gitlab.com:shadyLady/graffiti-splash.git

Als alles goed werkt wordt je om je wachtwoord gevraagd, dit is het wachtwoord van jouw persoonlijke key, dus degene die je net ingevuld hebt bij het 
aanmaken van de key. 

!!Eerste stap na het importeren van het project!!

Grote voordeel van werken met Git is dat het mogelijk is om in je eigen werkomgeving te werken, zonder het risico te lopen dat er dingen kapot gaan of 
verloren gaan in je project. De versie die je zojuist geimporteerd hebt is er een die in de master branch staat. Dit word de branch waarin we pas 
dingen gaan zetten wanneer we er volledig van overtuigd zijn dat dingen werken. 

Wanneer je in je eigen werkomgeving zit en naar Git kijkt, zie je dat er achter je locatie regel in het blauw master staat. Dit betekent dat je direct 
op de master werkt en alles wat je pusht ook direct op de master branch beland. Dit is natuurlijk niet de bedoeling. Daarom ga je eerst je eigen werkomgeving 
aanmaken door het volgende commando te gebruiken 

git branch development

In dit geval is het wel belangrijk dat je exact dezelfde naam typt als ik hierboven heb gedaan. Dit is omdat we deze branch zometeen gaan linken aan de 
development branch die ik heb aangemaakt in Gitlab. Dus ook niet per ongeluk met een hoofdletter beginnen, dit veroorzaakt merge conflicten, ofwel.. niet goed.

Nadat je de nieuwe branch hebt aangemaakt bevind je je nog steeds in de master branch. Om naar de nieuwe branch te verplaatsen gebruik je het volgende commando: 

git checkout development 

Als het goed is ben je nu verplaatst naar de development branch. DIt kun je uiteraard weer controleren in je locatie regel, waar nu als het goed is 
in het blauw in plaats van master, development staat. 

Als dit gelukt is en je hebt gecontroleerd of de naam van de branch overeen komt met de naam van de branch in gitlab, is de volgende stap het linken 
van je lokale branch aan de remote branch in gitlab. Dit doe je door het volgende commando te gebruiken: 

git push --set-upstream git@gitlab.com:shadyLady/graffiti-splash.git development

Nu komt het meest abstracte gedeelte van git, supermoeilijk om zo uit te leggen.. 

In feite werk je nu in je eigen omgeving, niets van wat je doet heeft invloed op het project dat zich op gitlab bevindt. Deze omgeving wordt je 
working directory genoemd. Wijzigingen die je wilt toevoegen aan gitlab moet je toevoegen aan je "Staging area" dit is een omgeving die nog steeds 
geen directe invloed heeft op het gitlab project, maar een verzameling van de items die toegevoegd gaan worden wanneer je ze doorvoert.

Om dit nu direct te kunnen ervaren kan je een tekstbestandje aanmaken in jou working directory. Controleer nog even goed of je inderdaad in de development 
branch zit voor je dit doet (wederom, mocht er iets kapot gaan is dit beter te doen in de development branch, zodat je zeker weet dat je uit de master 
branch nog een werkende versie kan halen) 

Zet iets randoms in je tekstbestandje en sla het op. Om je tekstbestand nu toe te kunnen voegen aan je staging area kun je verschillende dingen doen 

git add <file name> 		om specifieke bestanden toe te voegen 

git add .			om alle aangepaste bestanden toe te voegen 

Je kunt bekijken wat er in je staging area zit door het volgende commando in te typen 

git status 

Als het goed is zie je dan een hele hoop tekst verschijnen, waaronder de door jou toegevoegde bestanden in het groen. 

Om je staging area vast te leggen gebruik je hetvolgende commando : 

git commit -m "message" 

De message tussen quotes vervang je dan uiteraard door een bericht over de aard van de verandering die je gemaakt hebt. Zo kun je indien er iets fout 
gegaan is gemakkelijk zien welke versie je terug wilt halen. Dat, en als je geen message door geeft krijg je errors :P 

Als je wilt zien welke commits je allemaal gemaakt hebt kun je de volgende command gebruiken : 

git log --oneline 

Deze geeft je een overzicht van iedere commit en de daarbij horende ID, heel handig voor het terug gaan naar eerdere versies. Wat ik je overigens niet 
uit ga leggen, hiermee ga ik zelf constant de mist in, erg gevaarlijk.. mocht je terug moeten gaan, skype/bel/schreeuw naar mij 

Maar.. nu al dat werk, en je tekstbestand staat nog steeds niet op gitlab. Als je absoluut zeker weet dat alles wat je gedaan hebt werk kun je hetvolgende 
commando gebruiken om dan eindelijk je werk online te zetten  : 

git push git@gitlab.com:shadyLady/graffiti-splash.git development 

Nadat je je wachtwoord hebt ingevoerd worden je bestanden doorgestuurd en kan je op gitlab je wijzigingen zien! Awesome right? :D Wanneer je iets online 
gezet hebt lijkt het me handig dat je mij voorlopig even bericht, zodat ik kan zorgen dat als dat nodig is de wijzigingen doorgevoerd worden naar de 
master branche. 

Als je begint met werken is het altijd handig eerst de master naar je huidige master te pullen, zodat je altijd up to date bent. Om te zorgen dat je 
master en development branche altijd gelijk zijn moet je terwijl je in je master branche zit hetvolgende command invoeren 

git merge development

Dit zorgt ervoor dat alle afwijkingen in je development branche geupdate worden, waarna je weer naar je development branche kunt om verder te werken. 

