// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:3,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:True,hqlp:False,rprd:True,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:2865,x:34182,y:32687,varname:node_2865,prsc:2|diff-835-OUT,spec-3247-OUT,gloss-1813-OUT,normal-6890-OUT,alpha-6981-OUT;n:type:ShaderForge.SFN_Color,id:6665,x:32492,y:32589,ptovrint:False,ptlb:Color,ptin:_Color,varname:_Color,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Tex2d,id:7736,x:31984,y:32463,ptovrint:True,ptlb:Base Color,ptin:_MainTex,varname:_MainTex,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:a0edaef8cfa0ba442908bd820a395f29,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Slider,id:1813,x:33217,y:32795,ptovrint:False,ptlb:Gloss,ptin:_Gloss,varname:_Gloss,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Vector1,id:3247,x:33902,y:32691,varname:node_3247,prsc:2,v1:0;n:type:ShaderForge.SFN_Tex2d,id:3614,x:31656,y:33614,ptovrint:True,ptlb:Normal Map,ptin:_BumpMap,varname:_BumpMap,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:1c464b61dade1b9438ac32e0c9f0f200,ntxv:3,isnm:True|UVIN-7991-OUT;n:type:ShaderForge.SFN_FragmentPosition,id:7690,x:31082,y:33757,varname:node_7690,prsc:2;n:type:ShaderForge.SFN_Append,id:6694,x:31266,y:33788,varname:node_6694,prsc:2|A-7690-X,B-7690-Y;n:type:ShaderForge.SFN_ValueProperty,id:4712,x:31169,y:33654,ptovrint:False,ptlb:Scaling,ptin:_Scaling,varname:_Scaling,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.04;n:type:ShaderForge.SFN_Multiply,id:7991,x:31447,y:33741,varname:node_7991,prsc:2|A-4712-OUT,B-6694-OUT;n:type:ShaderForge.SFN_Append,id:6098,x:32290,y:33656,varname:node_6098,prsc:2|A-8108-OUT,B-3614-B;n:type:ShaderForge.SFN_Slider,id:9339,x:31656,y:33475,ptovrint:False,ptlb:Normal Intensity,ptin:_NormalIntensity,varname:_NormalIntensity,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:2;n:type:ShaderForge.SFN_Multiply,id:8108,x:32027,y:33500,varname:node_8108,prsc:2|A-9339-OUT,B-3897-OUT;n:type:ShaderForge.SFN_ComponentMask,id:3897,x:31845,y:33565,varname:node_3897,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-3614-RGB;n:type:ShaderForge.SFN_RgbToHsv,id:1887,x:32253,y:32459,varname:node_1887,prsc:2|IN-7736-RGB;n:type:ShaderForge.SFN_HsvToRgb,id:2495,x:32619,y:32455,varname:node_2495,prsc:2|H-2119-OUT,S-1887-SOUT,V-1887-VOUT;n:type:ShaderForge.SFN_Add,id:2119,x:32430,y:32365,varname:node_2119,prsc:2|A-8670-OUT,B-1887-HOUT;n:type:ShaderForge.SFN_Slider,id:8670,x:32061,y:32360,ptovrint:False,ptlb:Hue,ptin:_Hue,varname:_Hue,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Multiply,id:4056,x:33092,y:32575,varname:node_4056,prsc:2|A-2495-OUT,B-6665-RGB,C-3732-OUT;n:type:ShaderForge.SFN_LightColor,id:4620,x:32492,y:32729,varname:node_4620,prsc:2;n:type:ShaderForge.SFN_ToggleProperty,id:7134,x:32889,y:32806,ptovrint:False,ptlb:Lighting,ptin:_Lighting,varname:_LightColor,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False;n:type:ShaderForge.SFN_Lerp,id:3732,x:32889,y:32662,varname:node_3732,prsc:2|A-9464-OUT,B-9016-OUT,T-7134-OUT;n:type:ShaderForge.SFN_Vector1,id:9464,x:32619,y:32661,varname:node_9464,prsc:2,v1:1;n:type:ShaderForge.SFN_Lerp,id:6890,x:32838,y:33630,varname:node_6890,prsc:2|A-840-OUT,B-6098-OUT,T-8358-OUT;n:type:ShaderForge.SFN_ToggleProperty,id:8358,x:32431,y:33772,ptovrint:False,ptlb:Normal,ptin:_Normal,varname:_Normal,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False;n:type:ShaderForge.SFN_NormalVector,id:1314,x:32290,y:33510,prsc:2,pt:False;n:type:ShaderForge.SFN_ComponentMask,id:2257,x:32458,y:33385,varname:node_2257,prsc:2,cc1:2,cc2:-1,cc3:-1,cc4:-1|IN-1314-OUT;n:type:ShaderForge.SFN_Multiply,id:840,x:32637,y:33498,varname:node_840,prsc:2|A-2257-OUT,B-1314-OUT;n:type:ShaderForge.SFN_TexCoord,id:3274,x:32285,y:32025,varname:node_3274,prsc:2,uv:0;n:type:ShaderForge.SFN_OneMinus,id:6358,x:32525,y:32058,varname:node_6358,prsc:2|IN-3274-UVOUT;n:type:ShaderForge.SFN_ComponentMask,id:3135,x:32910,y:32149,varname:node_3135,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-4789-OUT;n:type:ShaderForge.SFN_RemapRange,id:6629,x:33151,y:32171,varname:node_6629,prsc:2,frmn:0,frmx:1,tomn:0.2,tomx:0.5|IN-3135-OUT;n:type:ShaderForge.SFN_Blend,id:5343,x:33434,y:32426,varname:node_5343,prsc:2,blmd:10,clmp:True|SRC-4056-OUT,DST-6629-OUT;n:type:ShaderForge.SFN_SwitchProperty,id:835,x:33678,y:32603,ptovrint:False,ptlb:Gradient Overlay,ptin:_GradientOverlay,varname:node_835,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-4056-OUT,B-5343-OUT;n:type:ShaderForge.SFN_Multiply,id:6981,x:33803,y:33007,varname:node_6981,prsc:2|A-3002-OUT,B-2122-OUT;n:type:ShaderForge.SFN_Slider,id:2122,x:33312,y:33084,ptovrint:False,ptlb:Opacity,ptin:_Opacity,varname:node_2122,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Multiply,id:3002,x:32835,y:32872,varname:node_3002,prsc:2|A-7736-A,B-4427-A;n:type:ShaderForge.SFN_VertexColor,id:4427,x:32561,y:33050,varname:node_4427,prsc:2;n:type:ShaderForge.SFN_Clamp01,id:9016,x:32673,y:32729,varname:node_9016,prsc:2|IN-4620-RGB;n:type:ShaderForge.SFN_ToggleProperty,id:3758,x:32559,y:32242,ptovrint:False,ptlb:Gradient Switch,ptin:_GradientSwitch,varname:node_3758,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:True;n:type:ShaderForge.SFN_Lerp,id:4789,x:32744,y:32178,varname:node_4789,prsc:2|A-6358-OUT,B-3274-UVOUT,T-3758-OUT;proporder:6665-7736-8670-7134-1813-8358-9339-4712-3614-835-2122-3758;pass:END;sub:END;*/

Shader "Shader Forge/Sprites with Lighting" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Base Color", 2D) = "white" {}
        _Hue ("Hue", Range(0, 1)) = 0
        [MaterialToggle] _Lighting ("Lighting", Float ) = 0
        _Gloss ("Gloss", Range(0, 1)) = 1
        [MaterialToggle] _Normal ("Normal", Float ) = 0
        _NormalIntensity ("Normal Intensity", Range(0, 2)) = 0
        _Scaling ("Scaling", Float ) = 0.04
        _BumpMap ("Normal Map", 2D) = "bump" {}
        [MaterialToggle] _GradientOverlay ("Gradient Overlay", Float ) = 0
        _Opacity ("Opacity", Range(0, 1)) = 1
        [MaterialToggle] _GradientSwitch ("Gradient Switch", Float ) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _Color;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Gloss;
            uniform sampler2D _BumpMap; uniform float4 _BumpMap_ST;
            uniform float _Scaling;
            uniform float _NormalIntensity;
            uniform float _Hue;
            uniform fixed _Lighting;
            uniform fixed _Normal;
            uniform fixed _GradientOverlay;
            uniform float _Opacity;
            uniform fixed _GradientSwitch;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                float4 vertexColor : COLOR;
                UNITY_FOG_COORDS(7)
                #if defined(LIGHTMAP_ON) || defined(UNITY_SHOULD_SAMPLE_SH)
                    float4 ambientOrLightmapUV : TEXCOORD8;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.vertexColor = v.vertexColor;
                #ifdef LIGHTMAP_ON
                    o.ambientOrLightmapUV.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
                    o.ambientOrLightmapUV.zw = 0;
                #elif UNITY_SHOULD_SAMPLE_SH
                #endif
                #ifdef DYNAMICLIGHTMAP_ON
                    o.ambientOrLightmapUV.zw = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
                #endif
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float2 node_7991 = (_Scaling*float2(i.posWorld.r,i.posWorld.g));
                float3 _BumpMap_var = UnpackNormal(tex2D(_BumpMap,TRANSFORM_TEX(node_7991, _BumpMap)));
                float3 normalLocal = lerp((i.normalDir.b*i.normalDir),float3((_NormalIntensity*_BumpMap_var.rgb.rg),_BumpMap_var.b),_Normal);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0+1.0);
/////// GI Data:
                UnityLight light;
                #ifdef LIGHTMAP_OFF
                    light.color = lightColor;
                    light.dir = lightDirection;
                    light.ndotl = LambertTerm (normalDirection, light.dir);
                #else
                    light.color = half3(0.f, 0.f, 0.f);
                    light.ndotl = 0.0f;
                    light.dir = half3(0.f, 0.f, 0.f);
                #endif
                UnityGIInput d;
                d.light = light;
                d.worldPos = i.posWorld.xyz;
                d.worldViewDir = viewDirection;
                d.atten = attenuation;
                #if defined(LIGHTMAP_ON) || defined(DYNAMICLIGHTMAP_ON)
                    d.ambient = 0;
                    d.lightmapUV = i.ambientOrLightmapUV;
                #else
                    d.ambient = i.ambientOrLightmapUV;
                #endif
                d.boxMax[0] = unity_SpecCube0_BoxMax;
                d.boxMin[0] = unity_SpecCube0_BoxMin;
                d.probePosition[0] = unity_SpecCube0_ProbePosition;
                d.probeHDR[0] = unity_SpecCube0_HDR;
                d.boxMax[1] = unity_SpecCube1_BoxMax;
                d.boxMin[1] = unity_SpecCube1_BoxMin;
                d.probePosition[1] = unity_SpecCube1_ProbePosition;
                d.probeHDR[1] = unity_SpecCube1_HDR;
                Unity_GlossyEnvironmentData ugls_en_data;
                ugls_en_data.roughness = 1.0 - gloss;
                ugls_en_data.reflUVW = viewReflectDirection;
                UnityGI gi = UnityGlobalIllumination(d, 1, normalDirection, ugls_en_data );
                lightDirection = gi.light.dir;
                lightColor = gi.light.color;
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float LdotH = max(0.0,dot(lightDirection, halfDirection));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float4 node_1887_k = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
                float4 node_1887_p = lerp(float4(float4(_MainTex_var.rgb,0.0).zy, node_1887_k.wz), float4(float4(_MainTex_var.rgb,0.0).yz, node_1887_k.xy), step(float4(_MainTex_var.rgb,0.0).z, float4(_MainTex_var.rgb,0.0).y));
                float4 node_1887_q = lerp(float4(node_1887_p.xyw, float4(_MainTex_var.rgb,0.0).x), float4(float4(_MainTex_var.rgb,0.0).x, node_1887_p.yzx), step(node_1887_p.x, float4(_MainTex_var.rgb,0.0).x));
                float node_1887_d = node_1887_q.x - min(node_1887_q.w, node_1887_q.y);
                float node_1887_e = 1.0e-10;
                float3 node_1887 = float3(abs(node_1887_q.z + (node_1887_q.w - node_1887_q.y) / (6.0 * node_1887_d + node_1887_e)), node_1887_d / (node_1887_q.x + node_1887_e), node_1887_q.x);;
                float node_9464 = 1.0;
                float3 node_4056 = ((lerp(float3(1,1,1),saturate(3.0*abs(1.0-2.0*frac((_Hue+node_1887.r)+float3(0.0,-1.0/3.0,1.0/3.0)))-1),node_1887.g)*node_1887.b)*_Color.rgb*lerp(float3(node_9464,node_9464,node_9464),saturate(_LightColor0.rgb),_Lighting));
                float3 diffuseColor = lerp( node_4056, saturate(( (lerp((1.0 - i.uv0),i.uv0,_GradientSwitch).g*0.3+0.2) > 0.5 ? (1.0-(1.0-2.0*((lerp((1.0 - i.uv0),i.uv0,_GradientSwitch).g*0.3+0.2)-0.5))*(1.0-node_4056)) : (2.0*(lerp((1.0 - i.uv0),i.uv0,_GradientSwitch).g*0.3+0.2)*node_4056) )), _GradientOverlay ); // Need this for specular when using metallic
                float specularMonochrome;
                float3 specularColor;
                diffuseColor = DiffuseAndSpecularFromMetallic( diffuseColor, 0.0, specularColor, specularMonochrome );
                specularMonochrome = 1-specularMonochrome;
                float NdotV = max(0.0,dot( normalDirection, viewDirection ));
                float NdotH = max(0.0,dot( normalDirection, halfDirection ));
                float VdotH = max(0.0,dot( viewDirection, halfDirection ));
                float visTerm = SmithBeckmannVisibilityTerm( NdotL, NdotV, 1.0-gloss );
                float normTerm = max(0.0, NDFBlinnPhongNormalizedTerm(NdotH, RoughnessToSpecPower(1.0-gloss)));
                float specularPBL = max(0, (NdotL*visTerm*normTerm) * (UNITY_PI / 4) );
                float3 directSpecular = 1 * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularPBL*lightColor*FresnelTerm(specularColor, LdotH);
                half grazingTerm = saturate( gloss + specularMonochrome );
                float3 indirectSpecular = (gi.indirect.specular);
                indirectSpecular *= FresnelLerp (specularColor, grazingTerm, NdotV);
                float3 specular = (directSpecular + indirectSpecular);
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                float3 directDiffuse = ((1 +(fd90 - 1)*pow((1.00001-NdotL), 5)) * (1 + (fd90 - 1)*pow((1.00001-NdotV), 5)) * NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += gi.indirect.diffuse;
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(finalColor,((_MainTex_var.a*i.vertexColor.a)*_Opacity));
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdadd
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _Color;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Gloss;
            uniform sampler2D _BumpMap; uniform float4 _BumpMap_ST;
            uniform float _Scaling;
            uniform float _NormalIntensity;
            uniform float _Hue;
            uniform fixed _Lighting;
            uniform fixed _Normal;
            uniform fixed _GradientOverlay;
            uniform float _Opacity;
            uniform fixed _GradientSwitch;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(7,8)
                UNITY_FOG_COORDS(9)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float2 node_7991 = (_Scaling*float2(i.posWorld.r,i.posWorld.g));
                float3 _BumpMap_var = UnpackNormal(tex2D(_BumpMap,TRANSFORM_TEX(node_7991, _BumpMap)));
                float3 normalLocal = lerp((i.normalDir.b*i.normalDir),float3((_NormalIntensity*_BumpMap_var.rgb.rg),_BumpMap_var.b),_Normal);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float LdotH = max(0.0,dot(lightDirection, halfDirection));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float4 node_1887_k = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
                float4 node_1887_p = lerp(float4(float4(_MainTex_var.rgb,0.0).zy, node_1887_k.wz), float4(float4(_MainTex_var.rgb,0.0).yz, node_1887_k.xy), step(float4(_MainTex_var.rgb,0.0).z, float4(_MainTex_var.rgb,0.0).y));
                float4 node_1887_q = lerp(float4(node_1887_p.xyw, float4(_MainTex_var.rgb,0.0).x), float4(float4(_MainTex_var.rgb,0.0).x, node_1887_p.yzx), step(node_1887_p.x, float4(_MainTex_var.rgb,0.0).x));
                float node_1887_d = node_1887_q.x - min(node_1887_q.w, node_1887_q.y);
                float node_1887_e = 1.0e-10;
                float3 node_1887 = float3(abs(node_1887_q.z + (node_1887_q.w - node_1887_q.y) / (6.0 * node_1887_d + node_1887_e)), node_1887_d / (node_1887_q.x + node_1887_e), node_1887_q.x);;
                float node_9464 = 1.0;
                float3 node_4056 = ((lerp(float3(1,1,1),saturate(3.0*abs(1.0-2.0*frac((_Hue+node_1887.r)+float3(0.0,-1.0/3.0,1.0/3.0)))-1),node_1887.g)*node_1887.b)*_Color.rgb*lerp(float3(node_9464,node_9464,node_9464),saturate(_LightColor0.rgb),_Lighting));
                float3 diffuseColor = lerp( node_4056, saturate(( (lerp((1.0 - i.uv0),i.uv0,_GradientSwitch).g*0.3+0.2) > 0.5 ? (1.0-(1.0-2.0*((lerp((1.0 - i.uv0),i.uv0,_GradientSwitch).g*0.3+0.2)-0.5))*(1.0-node_4056)) : (2.0*(lerp((1.0 - i.uv0),i.uv0,_GradientSwitch).g*0.3+0.2)*node_4056) )), _GradientOverlay ); // Need this for specular when using metallic
                float specularMonochrome;
                float3 specularColor;
                diffuseColor = DiffuseAndSpecularFromMetallic( diffuseColor, 0.0, specularColor, specularMonochrome );
                specularMonochrome = 1-specularMonochrome;
                float NdotV = max(0.0,dot( normalDirection, viewDirection ));
                float NdotH = max(0.0,dot( normalDirection, halfDirection ));
                float VdotH = max(0.0,dot( viewDirection, halfDirection ));
                float visTerm = SmithBeckmannVisibilityTerm( NdotL, NdotV, 1.0-gloss );
                float normTerm = max(0.0, NDFBlinnPhongNormalizedTerm(NdotH, RoughnessToSpecPower(1.0-gloss)));
                float specularPBL = max(0, (NdotL*visTerm*normTerm) * (UNITY_PI / 4) );
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularPBL*lightColor*FresnelTerm(specularColor, LdotH);
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                float3 directDiffuse = ((1 +(fd90 - 1)*pow((1.00001-NdotL), 5)) * (1 + (fd90 - 1)*pow((1.00001-NdotV), 5)) * NdotL) * attenColor;
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(finalColor * ((_MainTex_var.a*i.vertexColor.a)*_Opacity),0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "Meta"
            Tags {
                "LightMode"="Meta"
            }
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_META 1
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #include "UnityMetaPass.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _Color;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Gloss;
            uniform float _Hue;
            uniform fixed _Lighting;
            uniform fixed _GradientOverlay;
            uniform fixed _GradientSwitch;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : SV_Target {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 lightColor = _LightColor0.rgb;
                UnityMetaInput o;
                UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
                
                o.Emission = 0;
                
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float4 node_1887_k = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
                float4 node_1887_p = lerp(float4(float4(_MainTex_var.rgb,0.0).zy, node_1887_k.wz), float4(float4(_MainTex_var.rgb,0.0).yz, node_1887_k.xy), step(float4(_MainTex_var.rgb,0.0).z, float4(_MainTex_var.rgb,0.0).y));
                float4 node_1887_q = lerp(float4(node_1887_p.xyw, float4(_MainTex_var.rgb,0.0).x), float4(float4(_MainTex_var.rgb,0.0).x, node_1887_p.yzx), step(node_1887_p.x, float4(_MainTex_var.rgb,0.0).x));
                float node_1887_d = node_1887_q.x - min(node_1887_q.w, node_1887_q.y);
                float node_1887_e = 1.0e-10;
                float3 node_1887 = float3(abs(node_1887_q.z + (node_1887_q.w - node_1887_q.y) / (6.0 * node_1887_d + node_1887_e)), node_1887_d / (node_1887_q.x + node_1887_e), node_1887_q.x);;
                float node_9464 = 1.0;
                float3 node_4056 = ((lerp(float3(1,1,1),saturate(3.0*abs(1.0-2.0*frac((_Hue+node_1887.r)+float3(0.0,-1.0/3.0,1.0/3.0)))-1),node_1887.g)*node_1887.b)*_Color.rgb*lerp(float3(node_9464,node_9464,node_9464),saturate(_LightColor0.rgb),_Lighting));
                float3 diffColor = lerp( node_4056, saturate(( (lerp((1.0 - i.uv0),i.uv0,_GradientSwitch).g*0.3+0.2) > 0.5 ? (1.0-(1.0-2.0*((lerp((1.0 - i.uv0),i.uv0,_GradientSwitch).g*0.3+0.2)-0.5))*(1.0-node_4056)) : (2.0*(lerp((1.0 - i.uv0),i.uv0,_GradientSwitch).g*0.3+0.2)*node_4056) )), _GradientOverlay );
                float specularMonochrome;
                float3 specColor;
                diffColor = DiffuseAndSpecularFromMetallic( diffColor, 0.0, specColor, specularMonochrome );
                float roughness = 1.0 - _Gloss;
                o.Albedo = diffColor + specColor * roughness * roughness * 0.5;
                
                return UnityMetaFragment( o );
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
