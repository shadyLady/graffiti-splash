// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:1,cusa:True,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:True,tesm:0,olmd:1,culm:2,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:1873,x:33433,y:32719,varname:node_1873,prsc:2|emission-1749-OUT,alpha-603-OUT;n:type:ShaderForge.SFN_Tex2d,id:4805,x:32099,y:32720,varname:_MainTex_copy,prsc:2,ntxv:0,isnm:False|UVIN-6285-UVOUT,TEX-8140-TEX;n:type:ShaderForge.SFN_Multiply,id:1086,x:32812,y:32818,cmnt:RGB,varname:node_1086,prsc:2|A-8181-OUT,B-5983-RGB,C-5376-RGB;n:type:ShaderForge.SFN_Color,id:5983,x:32557,y:32647,ptovrint:False,ptlb:Color,ptin:_Color,varname:_Color_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_VertexColor,id:5376,x:32544,y:32886,varname:node_5376,prsc:2;n:type:ShaderForge.SFN_Multiply,id:1749,x:33025,y:32818,cmnt:Premultiply Alpha,varname:node_1749,prsc:2|A-1086-OUT,B-603-OUT;n:type:ShaderForge.SFN_Multiply,id:603,x:32812,y:32992,cmnt:A,varname:node_603,prsc:2|A-5593-A,B-5983-A,C-5376-A,D-4805-A;n:type:ShaderForge.SFN_TexCoord,id:5230,x:31344,y:32561,varname:node_5230,prsc:2,uv:0;n:type:ShaderForge.SFN_UVTile,id:6285,x:31688,y:32682,varname:node_6285,prsc:2|UVIN-5230-UVOUT,WDT-6236-OUT,HGT-6236-OUT,TILE-5627-OUT;n:type:ShaderForge.SFN_Vector1,id:6236,x:31335,y:32718,varname:node_6236,prsc:2,v1:2;n:type:ShaderForge.SFN_Add,id:3309,x:31164,y:33087,varname:node_3309,prsc:2|A-2589-OUT,B-3092-OUT;n:type:ShaderForge.SFN_Vector1,id:3092,x:30985,y:33103,varname:node_3092,prsc:2,v1:1;n:type:ShaderForge.SFN_UVTile,id:6075,x:31688,y:32865,varname:node_6075,prsc:2|UVIN-5230-UVOUT,WDT-6236-OUT,HGT-6236-OUT,TILE-8893-OUT;n:type:ShaderForge.SFN_Tex2d,id:3657,x:32099,y:32852,varname:node_3657,prsc:2,ntxv:0,isnm:False|UVIN-6075-UVOUT,TEX-8140-TEX;n:type:ShaderForge.SFN_Lerp,id:3409,x:32330,y:32782,varname:node_3409,prsc:2|A-4805-RGB,B-3657-RGB,T-7928-OUT;n:type:ShaderForge.SFN_Tex2dAsset,id:8140,x:31855,y:32770,ptovrint:False,ptlb:SpriteSheet,ptin:_SpriteSheet,varname:node_8140,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Trunc,id:2589,x:30895,y:32924,varname:node_2589,prsc:2|IN-2230-OUT;n:type:ShaderForge.SFN_Frac,id:7928,x:31483,y:33232,varname:node_7928,prsc:2|IN-2230-OUT;n:type:ShaderForge.SFN_ConstantClamp,id:8893,x:31407,y:33068,varname:node_8893,prsc:2,min:0,max:3|IN-3309-OUT;n:type:ShaderForge.SFN_ConstantClamp,id:5627,x:31335,y:32879,varname:node_5627,prsc:2,min:0,max:3|IN-2589-OUT;n:type:ShaderForge.SFN_Time,id:1350,x:30989,y:33753,varname:node_1350,prsc:2;n:type:ShaderForge.SFN_Lerp,id:7914,x:32097,y:33537,varname:node_7914,prsc:2|A-6240-OUT,B-5910-OUT,T-5731-OUT;n:type:ShaderForge.SFN_Vector1,id:6240,x:31823,y:33515,varname:node_6240,prsc:2,v1:0;n:type:ShaderForge.SFN_Vector1,id:2395,x:31112,y:33484,varname:node_2395,prsc:2,v1:3;n:type:ShaderForge.SFN_Color,id:2388,x:31910,y:33868,ptovrint:False,ptlb:Color Full,ptin:_ColorFull,varname:node_2388,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.08965492,c3:1,c4:1;n:type:ShaderForge.SFN_ConstantClamp,id:5560,x:31102,y:33337,varname:node_5560,prsc:2,min:3,max:4|IN-2230-OUT;n:type:ShaderForge.SFN_Subtract,id:8664,x:31382,y:33398,varname:node_8664,prsc:2|A-5560-OUT,B-2395-OUT;n:type:ShaderForge.SFN_Color,id:5637,x:31601,y:33450,ptovrint:False,ptlb:Color Setup,ptin:_ColorSetup,varname:node_5637,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Lerp,id:7449,x:32417,y:33259,varname:node_7449,prsc:2|A-7914-OUT,B-4514-OUT,T-2138-OUT;n:type:ShaderForge.SFN_Sin,id:7040,x:31465,y:33758,varname:node_7040,prsc:2|IN-4144-OUT;n:type:ShaderForge.SFN_RemapRange,id:3652,x:31699,y:33814,varname:node_3652,prsc:2,frmn:-1,frmx:1,tomn:0,tomx:1|IN-7040-OUT;n:type:ShaderForge.SFN_Trunc,id:2138,x:31975,y:33354,varname:node_2138,prsc:2|IN-8664-OUT;n:type:ShaderForge.SFN_Multiply,id:4514,x:32110,y:33745,varname:node_4514,prsc:2|A-2388-RGB,B-3652-OUT;n:type:ShaderForge.SFN_Slider,id:2230,x:30483,y:33146,ptovrint:False,ptlb:Stage,ptin:_Stage,varname:node_2230,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:4;n:type:ShaderForge.SFN_Multiply,id:4144,x:31216,y:33719,varname:node_4144,prsc:2|A-2347-OUT,B-1350-T;n:type:ShaderForge.SFN_Slider,id:2347,x:30915,y:33640,ptovrint:False,ptlb:Speed,ptin:_Speed,varname:node_2347,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:2.5,max:5;n:type:ShaderForge.SFN_Tex2d,id:5593,x:32781,y:32480,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_5593,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_RemapRange,id:5731,x:30815,y:33448,varname:node_5731,prsc:2,frmn:0,frmx:4,tomn:0,tomx:1|IN-2230-OUT;n:type:ShaderForge.SFN_Add,id:8181,x:32628,y:33138,varname:node_8181,prsc:2|A-3409-OUT,B-7449-OUT;n:type:ShaderForge.SFN_Lerp,id:5910,x:31865,y:33630,varname:node_5910,prsc:2|A-5637-RGB,B-2388-RGB,T-8664-OUT;proporder:5983-8140-5637-2388-2230-2347-5593;pass:END;sub:END;*/

Shader "Shader Forge/HUD Shader" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _SpriteSheet ("SpriteSheet", 2D) = "white" {}
        _ColorSetup ("Color Setup", Color) = (1,0,0,1)
        _ColorFull ("Color Full", Color) = (0,0.08965492,1,1)
        _Stage ("Stage", Range(0, 4)) = 1
        _Speed ("Speed", Range(0, 5)) = 2.5
        _MainTex ("MainTex", 2D) = "white" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
            "CanUseSpriteAtlas"="True"
            "PreviewType"="Plane"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _Color;
            uniform sampler2D _SpriteSheet; uniform float4 _SpriteSheet_ST;
            uniform float4 _ColorFull;
            uniform float4 _ColorSetup;
            uniform float _Stage;
            uniform float _Speed;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                #ifdef PIXELSNAP_ON
                    o.pos = UnityPixelSnap(o.pos);
                #endif
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
////// Emissive:
                float node_6236 = 2.0;
                float node_2589 = trunc(_Stage);
                float node_5627 = clamp(node_2589,0,3);
                float2 node_6285_tc_rcp = float2(1.0,1.0)/float2( node_6236, node_6236 );
                float node_6285_ty = floor(node_5627 * node_6285_tc_rcp.x);
                float node_6285_tx = node_5627 - node_6236 * node_6285_ty;
                float2 node_6285 = (i.uv0 + float2(node_6285_tx, node_6285_ty)) * node_6285_tc_rcp;
                float4 _MainTex_copy = tex2D(_SpriteSheet,TRANSFORM_TEX(node_6285, _SpriteSheet));
                float node_8893 = clamp((node_2589+1.0),0,3);
                float2 node_6075_tc_rcp = float2(1.0,1.0)/float2( node_6236, node_6236 );
                float node_6075_ty = floor(node_8893 * node_6075_tc_rcp.x);
                float node_6075_tx = node_8893 - node_6236 * node_6075_ty;
                float2 node_6075 = (i.uv0 + float2(node_6075_tx, node_6075_ty)) * node_6075_tc_rcp;
                float4 node_3657 = tex2D(_SpriteSheet,TRANSFORM_TEX(node_6075, _SpriteSheet));
                float node_6240 = 0.0;
                float node_8664 = (clamp(_Stage,3,4)-3.0);
                float4 node_1350 = _Time + _TimeEditor;
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float node_603 = (_MainTex_var.a*_Color.a*i.vertexColor.a*_MainTex_copy.a); // A
                float3 emissive = (((lerp(_MainTex_copy.rgb,node_3657.rgb,frac(_Stage))+lerp(lerp(float3(node_6240,node_6240,node_6240),lerp(_ColorSetup.rgb,_ColorFull.rgb,node_8664),(_Stage*0.25+0.0)),(_ColorFull.rgb*(sin((_Speed*node_1350.g))*0.5+0.5)),trunc(node_8664)))*_Color.rgb*i.vertexColor.rgb)*node_603);
                float3 finalColor = emissive;
                return fixed4(finalColor,node_603);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
