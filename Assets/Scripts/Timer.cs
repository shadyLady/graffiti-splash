﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Timer : MonoBehaviour
{

    [SerializeField] private Sprite[] _countdownSprites;
    [SerializeField] private Text _levelTimerText; 

    private float _levelTime = 60.0f;

    public IEnumerator StartCountDown(float duration, int repeats, System.Action callBack)
    {
        gameObject.SetActive(true);

        while (repeats >= 0)
        {
            gameObject.GetComponent<Image>().sprite = _countdownSprites[repeats];
            repeats--;
            yield return new WaitForSeconds(duration);
        }

        gameObject.SetActive(false);

        if (callBack != null)
            callBack();
    }
    public IEnumerator StartLevelTimer(System.Action callBack)
    {
        while (_levelTime >= 0)
        {
            _levelTime--;
            _levelTimerText.text = _levelTime.ToString();
            yield return new WaitForSeconds(1.0f);
        }
    }
}
