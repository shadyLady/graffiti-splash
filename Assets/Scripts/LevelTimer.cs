﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelTimer : MonoBehaviour
{
    private Slider _timeSlider;

    [SerializeField] private float _levelTime;
    private float _currentTime  = 0;

    [SerializeField] private Text _timeText;
    
    private void Awake()
    {
        _timeSlider = GetComponent<Slider>();
        GameManager.Instance.OnLevelStart += StartTimer;
    }

    private void StartTimer()
    {
        StopAllCoroutines();
        StartCoroutine(RunTimer());
    }

    private IEnumerator RunTimer()
    {
        while(_currentTime < _levelTime)
        {
            _currentTime += Time.deltaTime;
            _timeSlider.value = _currentTime;
            _timeText.text = (_levelTime - Mathf.RoundToInt(_currentTime)).ToString();
            yield return new WaitForEndOfFrame();
        }

        _currentTime = 0;
        _timeSlider.value = _currentTime;

        GameManager.Instance.OnGameStartEvent += StartTimer;
        GameManager.Instance.EndLevel();
        yield return null;
    }

}
