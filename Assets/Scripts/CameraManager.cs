﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour
{
    private Camera _camera;
    private Vector3 _position; 

    private void Awake()
    {
        if (_camera != null)
            return;

        _camera = FindObjectOfType<Camera>();
        _position = _camera.transform.localPosition;
    }
    public void ShakeItBaby(float intensity, float decrement)
    {
        StartCoroutine(ScreenShake(intensity, decrement));
    }
    private IEnumerator ScreenShake(float intensity, float decrement)
    {
        float tmp = intensity;

        while (tmp > 0)
        {
            _camera.transform.localPosition = _position + Random.insideUnitSphere * tmp;
            tmp -= Time.deltaTime * decrement;
            yield return new WaitForEndOfFrame();
        }

        _camera.transform.localPosition = _position;
        yield return null;
    }
}
