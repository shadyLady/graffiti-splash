﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScoreManager 
{
    private float[] _playerScores;
    public float[] PlayerScores
    {
        get { return _playerScores; }
    }

    private int _winningPlayer;
    public int WinningPlayer
    {
        get { return _winningPlayer; }
    }

    private int _losingPlayer; 
    public int LosingPlayer
    {
        get { return _losingPlayer; }
    }

    private event VoidDelegate _updatedScore;
    public VoidDelegate UpdatedScore
    {
        get { return _updatedScore; }
        set { _updatedScore = value; }
    }

    public ScoreManager()
    {
        GameManager.Instance.SplashManager.SplashAddedEvent += UpdateScores;
        GameManager.Instance.OnGameEndEvent += DefineWinner;
        _playerScores = new float[GameManager.Instance.PlayerData.Length];
    }
    private void UpdateScores()
    {
        List<Splash> activeSplashes = GameManager.Instance.SplashManager.GetSplashList();

        float totalPaintedSurface = 0; 

        float[] playerPaintedSurface = new float[2];

        for (int i = 0; i < activeSplashes.Count; i++)
        {
            totalPaintedSurface += activeSplashes[i].SurfaceSize;

            if (activeSplashes[i].PlayerID == 0)
                playerPaintedSurface[1] += activeSplashes[i].SurfaceSize;
            else playerPaintedSurface[0] += activeSplashes[i].SurfaceSize;    
        }

        _playerScores[0] = playerPaintedSurface[0] / totalPaintedSurface * 100;
        _playerScores[1] = playerPaintedSurface[1] / totalPaintedSurface * 100;

        if (_updatedScore != null)
            _updatedScore();
    }
    private void DefineWinner()
    {
        if (_playerScores[0] > _playerScores[1])
        {
            _winningPlayer = 0;
            _losingPlayer = 1;
        }
        else  if(_playerScores[1] > _playerScores[0])
        {
            _winningPlayer = 1;
            _losingPlayer = 0;
        }

        Debug.Log("Player 0 " + _playerScores[0] + " Player 1 " + _playerScores[1] + " winner " + _winningPlayer);

        GameManager.Instance.Players[_losingPlayer].Animator.SetTrigger("Lost");
        GameManager.Instance.Players[_losingPlayer].AnimatorHead.SetTrigger("Lost");
    }
}
