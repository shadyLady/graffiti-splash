﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

public class SplashManager 
{
    private event VoidDelegate _splashAddedEvent; 
    public VoidDelegate SplashAddedEvent
    {
        get { return _splashAddedEvent; }
        set { _splashAddedEvent = value; }
    }

    //private List<Splash> _regularSplashes;
    //private List<Splash> _largeSplashes;

    private Splash[] _regularSplash;
    private Splash[] _largeSplash; 

    private List<Splash> _activeSplashes = new List<Splash>();

    public SplashManager()
    {
        LoadSplashes();

        GameManager.Instance.OnLevelReload += ClearLevel;
        GameManager.Instance.OnMenuLoaded += ClearLevel;
    }

    public void LoadSplashes()
    {
        _regularSplash = Resources.LoadAll<Splash>("Prefabs/Splashes/Regular");
        _largeSplash = Resources.LoadAll<Splash>("Prefabs/Splashes/Large");
        /*
        _regularSplashes = new List<Splash>();
        _largeSplashes = new List<Splash>();

        GameObject splashContainer = new GameObject();
        splashContainer.name = "SplashContainer";

        for (int i = 0; i < 50; i++)
        {
            _regularSplashes.Add(MonoBehaviour.Instantiate(regular[Random.Range(0, regular.Length)]));
            _regularSplashes[i].transform.parent = splashContainer.transform;
            _regularSplashes[i].name = "Regular_" + i;

            _largeSplashes.Add(MonoBehaviour.Instantiate(large[Random.Range(0, large.Length)]));
            _largeSplashes[i].transform.parent = splashContainer.transform;
            _largeSplashes[i].name = "Large_" + i;
            
        }*/
    }

    public void ActivateSplash(int playerID, string type, Vector3 position, int direction, float size)
    {
        Splash splash = null; 

        switch(type)
        {
            case "Regular": splash = MonoBehaviour.Instantiate(_regularSplash[Random.Range(0, _regularSplash.Length)]); break;
            case "Large":   splash = MonoBehaviour.Instantiate(_largeSplash[Random.Range(0, _largeSplash.Length)]);     break;
        }

        float rot = 0;

        if (direction == -1)
            rot = -180;
        else rot = 0;

        Quaternion rotation = Quaternion.Euler(0.0f, rot, 0.0f);
        Vector3 scale = new Vector3(splash.transform.localScale.x * size, splash.transform.localScale.y * size, 1.0f);
        Color color = GameManager.Instance.PlayerData[playerID].PlayerColor;

        splash.Activate(playerID, position, scale, rotation, color, this);

        _activeSplashes.Add(splash);
        CheckOverlap(splash);

        if (_splashAddedEvent != null)
            _splashAddedEvent();
    }

    private Splash LoadSplashFromList(List<Splash> splashList)
    {
        for(int i = 0; i < splashList.Count; i++)
        {
            if (!splashList[i].isActiveAndEnabled)
            {
                return splashList[i];
            }
        }

        Debug.Log("No available splashes in list " + splashList);
        return null;

    }
    private void CheckOverlap(Splash splash)
    {
        if (_activeSplashes.Count <= 1)
            return;

        Vector2 overlap = Vector2.zero; 

        for(int i = 0; i < _activeSplashes.Count; i++)
        {
            if (_activeSplashes[i] == splash)
                return;

            if(splash.Min.x >= _activeSplashes[i].Min.x && splash.Min.x <= _activeSplashes[i].Max.x) { overlap.x = _activeSplashes[i].Max.x - splash.Min.x; }
            else if(splash.Max.x >= _activeSplashes[i].Min.x && splash.Max.x <= _activeSplashes[i].Max.x) { overlap.x = splash.Max.x - _activeSplashes[i].Min.x; }

            if(splash.Min.y >= _activeSplashes[i].Min.y && splash.Min.y <= _activeSplashes[i].Max.y) { overlap.y = _activeSplashes[i].Max.y - splash.Min.y; }
            else if(splash.Max.y >= _activeSplashes[i].Min.y && splash.Max.y <= _activeSplashes[i].Max.y) { overlap.y = splash.Max.y - _activeSplashes[i].Min.y; }

            if (overlap.x != 0 && overlap.y != 0)
            {
                _activeSplashes[i].SurfaceSize -= overlap.x * overlap.y;

                FixSortingOrder(splash, _activeSplashes[i]);
            }


            
        }
    }
    public void RemoveSplashFromActiveList(Splash splash)
    {
        Debug.Log("Removing splash " + splash.name);

        for(int i = 0; i < _activeSplashes.Count; i++)
        {
            if (_activeSplashes[i].Equals(splash))
            {
                _activeSplashes.Remove(splash);
                break;
            }
        }
    }
    private void FixSortingOrder(Splash a, Splash b)
    {
        a.GetComponent<SpriteRenderer>().sortingOrder = 1;
        b.GetComponent<SpriteRenderer>().sortingOrder = 0;
    }
    public List<Splash> GetSplashList()
    {
        return _activeSplashes;
    }

    public void ClearLevel()
    {
        Debug.Log("Clearing level");
        for(int i = 0; i < _activeSplashes.Count; i++)
        {
            MonoBehaviour.Destroy(_activeSplashes[i].gameObject);
        }

        _activeSplashes.Clear();
    }
}
