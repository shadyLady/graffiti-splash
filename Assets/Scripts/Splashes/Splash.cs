﻿using UnityEngine;
using System.Collections;

public class Splash : MonoBehaviour
{
    private SplashManager _splashManager; 

    private int _playerID; 
    public int PlayerID
    {
        get { return _playerID; }
    }

    [SerializeField]
    private Vector2 _min; 
    public Vector2 Min
    {
        get { return _min; }
    }

    [SerializeField]
    private Vector2 _max; 
    public Vector2 Max
    {
        get { return _max; }
    }

    [SerializeField]
    private float _surfaceSize; 
    public float SurfaceSize
    {
        get { return _surfaceSize; }
        set
        {
            _surfaceSize = value;

            if (_surfaceSize < 0)
                _surfaceSize = 0;
        }
    }

    public void Activate(int id, Vector3 position, Vector3 scale, Quaternion rotation, Color color, SplashManager splashManager)
    {
        _playerID = id;
        _splashManager = splashManager;

        transform.position = position;
        transform.rotation = rotation;
        transform.localScale = scale;

        SpriteRenderer renderer = GetComponent<SpriteRenderer>();

        renderer.material.SetColor("_PaintColor", color);
        Bounds bounds = renderer.bounds;

        _min = bounds.min;
        _max = bounds.max; 

        _surfaceSize = bounds.size.x * bounds.size.y;

        gameObject.SetActive(true);
    }

    private void Deactivate()
    {
        Reset();
        _splashManager.RemoveSplashFromActiveList(this);
        gameObject.SetActive(false);
    }

    private void Reset()
    {
        transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
    }
}
