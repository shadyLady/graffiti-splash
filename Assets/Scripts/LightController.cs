﻿using UnityEngine;
using System.Collections;

public class LightController : MonoBehaviour
{ 
    private Light _light;

    private void Awake()
    {
        _light = GetComponent<Light>();
    }

    public IEnumerator SwitchOn(float _maxDuration, int repeats)
    {
        float duration = Random.Range(0, _maxDuration);

        while (repeats >= 0)
        {
            _light.enabled = !_light.enabled;

            duration = Random.Range(0, _maxDuration);
            repeats--;

            yield return new WaitForSeconds(duration);
        }
    }
}
