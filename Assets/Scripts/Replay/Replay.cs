﻿using UnityEngine;
using System;
using System.IO;
using System.Xml;
using System.Diagnostics; 
using System.Collections;
using System.Collections.Generic; 

public class Replay : MonoBehaviour
{
    [SerializeField] private string _filePath;
    [SerializeField] private List<string> _commandList;

    [SerializeField] private Player[] _players;
    private InputData[] _prevInput;
    [SerializeField] private InputData[] _replayInput; 

    [SerializeField] private bool _record = false;
    private IEnumerator _replayCoroutine;

    private float _currentTime = 0.0f;
    private Stopwatch _stopWatch = new Stopwatch();
    private bool _recording = false; 


    private void Start()
    {
        if(_record)
        {
            GameManager.Instance.OnLevelStart += StartRecording;
            GameManager.Instance.OnGameEndEvent += StopRecording;
        }
        else
        {
            GameManager.Instance.OnReplayStart += StartReplay; 
        }
    }

    private IEnumerator DoReplay()
    {
        XmlDocument doc = LoadXML();

        XmlElement action = doc.DocumentElement;

        int playerID = -1;
        string cmd = "";
        string cmdVal = "";
        _stopWatch.Reset();
        _stopWatch.Start();
        
        for (int i = 0; i < action.ChildNodes.Count; i++)
        {
            XmlNode node = action.ChildNodes[i];

            //timestamp = float.Parse(node.Attributes["timestamp"].Value);
            TimeSpan t = TimeSpan.Parse(node.Attributes["timestamp"].Value);
            playerID = int.Parse(node.Attributes["playerid"].Value);
            cmd = node.Attributes["action"].Value;
            cmdVal = node.Attributes["value"].Value;


            TimeSpan time = _stopWatch.Elapsed;

            while (time < t)
            {
                time = _stopWatch.Elapsed;
                yield return new WaitForEndOfFrame();
            }

            switch(cmd)
            {
                case "horizontal":
                    {
                        _replayInput[playerID].Horizontal = int.Parse(cmdVal);
                    }
                    break;
                case "jump":
                    {
                        _replayInput[playerID].Jump = bool.Parse(cmdVal);
                    }
                    break;
                case "quick-attack":
                    {
                        _replayInput[playerID].QuickAttack = bool.Parse(cmdVal);
                    }
                    break;
                case "charge-attack":
                    {
                        _replayInput[playerID].KeyboardCharge = bool.Parse(cmdVal);
                    }
                    break;
            }

            _players[playerID].InputData = _replayInput[playerID];  
        }

        GameManager.Instance.StopReplay();
        yield return null; 
    }

    private void StartRecording()
    {
        _stopWatch.Reset();
        _stopWatch.Start();
        _players = GameManager.Instance.Players;
        _prevInput = new InputData[_players.Length];
        _recording = true; 

        for (int i = 0; i < _players.Length; i++)
        {
            _prevInput[i] = new InputData();
        }

        _commandList = new List<string>();
    }

    private void Update()
    {
        if (_recording)
            Record();

        if(_replayCoroutine != null)
        {
            if (Input.GetMouseButtonDown(0))
                StopReplay();
        }
    }

    private void Record()
    {
        TimeSpan time = _stopWatch.Elapsed;

        for (int i = 0; i < _players.Length; i++)
        {
            InputData inputData = _players[i].InputData;

            if (inputData.KeyboardHorizontal != _prevInput[i].KeyboardHorizontal) _commandList.Add("timestamp=\"" + time + "\" playerid=\"" + _players[i].PlayerID + "\" action=\"horizontal\" value=\"" + inputData.KeyboardHorizontal + "\"");
            if (inputData.KeyboardJump != _prevInput[i].KeyboardJump) _commandList.Add("timestamp=\"" + time + "\" playerid=\"" + _players[i].PlayerID + "\" action=\"jump\" value=\"" + inputData.KeyboardJump + "\"");
            if (inputData.KeyboardQuickAttack != _prevInput[i].KeyboardQuickAttack) _commandList.Add("timestamp=\"" + time + "\" playerid=\"" + _players[i].PlayerID + "\" action=\"quick-attack\" value=\"" + inputData.KeyboardQuickAttack + "\"");
            if (inputData.KeyboardCharge != _prevInput[i].KeyboardCharge) _commandList.Add("timestamp=\"" + time + "\" playerid=\"" + _players[i].PlayerID + "\" action=\"charge-attack\" value=\"" + inputData.KeyboardCharge + "\"");

            _prevInput[i] = inputData;
        }
    }

    private void SaveRecording()
    {
        StreamWriter writer = File.CreateText(Application.dataPath + "/" + _filePath);
        writer.WriteLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        writer.WriteLine("<replay>");
            for(int i = 0; i < _commandList.Count; i++)
            {
                writer.WriteLine("\t<action " + _commandList[i] + "/>");
            }
        writer.WriteLine("</replay>");
        writer.Close();
    }

    private void StopRecording()
    {
        if(_record && _commandList.Count > 0)
            SaveRecording();
    }

    private void OnDestroy()
    {
        if(_record && _commandList.Count > 0)
            StopRecording();

        if (_replayCoroutine != null)
            StopReplay();
    }

    private void StartReplay()
    {
        if (_replayCoroutine != null)
            return;

        _players = GameManager.Instance.Players;
        _replayInput = new InputData[_players.Length];

        for(int i = 0; i < _replayInput.Length; i++)
        {
            _replayInput[i] = new InputData();
        }

        _replayCoroutine = DoReplay();
        StartCoroutine(_replayCoroutine);
    }

    private void StopReplay()
    {
        if(_replayCoroutine != null)
        {
            StopCoroutine(_replayCoroutine);
            _replayCoroutine = null;
            GameManager.Instance.StopReplay();
        }
    }

    
    private XmlDocument LoadXML()
    {
        XmlDocument doc = new XmlDocument();

        try { doc.Load(Application.dataPath + "/" + _filePath); }
        catch (Exception e)
        {
            UnityEngine.Debug.Log(e.Message);
        }

        return doc;
    }
}
