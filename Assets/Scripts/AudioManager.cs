﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour
{
    [SerializeField] private AudioSource _audio;
    [SerializeField] private AudioSource _audioClips;

    [SerializeField] private AudioClip[] _musicClips;
    [SerializeField] private AudioClip[] _clips;

    [SerializeField] private float _currentVolume = 0.2f;
    [SerializeField] private float _newClipVolume = 0.0f;

    private bool _playingNextTrack = false;
    private bool _switching = false;

    private void Awake()
    {
        _audio = GetComponent<AudioSource>();
        _audio.clip = _musicClips[0];
        _audio.Play(); 
    }

    public void PlayClip(int id)
    {
        _audioClips.PlayOneShot(_clips[id]);
    }
    public void PlayMusic(int id)
    {
        _audio.clip = _musicClips[0];
        _audio.Play();
    }
    private void Update()
    {
        if(_switching)
        {
            Debug.Log("Switching");
            SwitchTrack(1);
        }
        
        if(_newClipVolume >= 0.2f)
        {
            _currentVolume = _newClipVolume;
            _newClipVolume = 0.0f;
            _switching = false;
        }
    }
    public void StartSwitch(int id)
    {
        _switching = true;
    }
    private void SwitchTrack(int id)
    {
        FadeOut();

        if(_currentVolume < 0.1f)
        {
            if(!_playingNextTrack)
            {
                _playingNextTrack = true;

                _audio.clip = _musicClips[id];
                _audio.Play();
            }

            FadeIn();
        }
    }
    private void FadeOut()
    {
        if(_currentVolume > 0.1f)
        {
            _currentVolume -= Time.deltaTime * 0.9f;
            _audio.volume = _currentVolume;
        }
    }
    private void FadeIn()
    {
        if(_newClipVolume < 0.2f)
        {
            _newClipVolume += Time.deltaTime * 0.7f;
            _audio.volume = _newClipVolume;
        }
    }
}
