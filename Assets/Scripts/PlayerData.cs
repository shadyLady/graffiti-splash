﻿using UnityEngine;
using System.Collections;

public struct PlayerData
{
    public Player Prefab;
    public int PlayerID;
    public Color PlayerColor;

    public PlayerData(int id, Player prefab, Color color) : this()
    {
        PlayerID = id;
        Prefab = prefab;
        PlayerColor = color;
    }
}
