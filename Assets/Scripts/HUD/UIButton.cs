﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIButton : MonoBehaviour
{
    private Button _button; 

    private void Awake()
    {
        _button = GetComponent<Button>();
           
    }
    
    public void PlayHoverSound()
    {
        GameManager.Instance.AudioManager.PlayClip(Random.Range(0, 3));
    }
}
