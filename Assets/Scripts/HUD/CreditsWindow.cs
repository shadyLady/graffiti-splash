﻿using UnityEngine;
using System.Collections;

public class CreditsWindow : MonoBehaviour
{
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space) || Input.GetButtonDown("Cancel"))
        {
            InterfaceManager manager = FindObjectOfType<InterfaceManager>();
            manager.HideCredits();
        }
    }

}
