﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelCountdown : MonoBehaviour
{

    [SerializeField]
    private Sprite[] _countdownSprites;

    public IEnumerator StartCountDown(float duration, int repeats, System.Action callBack)
    {
        GameManager.Instance.AudioManager.PlayClip(7);
        gameObject.SetActive(true);

        while (repeats >= 0)
        {
            gameObject.GetComponent<Image>().sprite = _countdownSprites[repeats];
            repeats--;
            yield return new WaitForSeconds(duration);
        }

        gameObject.SetActive(false);

        if (callBack != null)
            callBack();
    }
}
