﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ResultWindow : MonoBehaviour
{
    public Image _spriteRenderer;

    private void Start()
    {
        int winner = GameManager.Instance.ScoreManager.WinningPlayer;
        _spriteRenderer.material.SetFloat("_Hue", GameManager.Instance.Players[winner].GetComponent<SpriteRenderer>().material.GetFloat("_Hue"));
    }
}
