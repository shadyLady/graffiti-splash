﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreBar : MonoBehaviour
{
    private ScoreManager _scoreManager;
    private Image _scoreBar;

    private float _lerpTime = 0.5f;
    private float _currentLerpTime = 0.0f;
    private float _amountToLerp = 0.0f;

    private bool _isLerping = false;

    [SerializeField] private float _currentValue = 0.0f;
    [SerializeField] private float _newValue = 0.0f; 

    private void Awake()
    {
        _scoreBar = GetComponent<Image>();
        GameManager.Instance.OnLevelStart += FindScoreManager;
    }
    private void UpdateScoreBar()
    {
        Debug.Log("Updated scorebar");
        _currentValue = _scoreBar.material.GetFloat("_Score");
        _newValue = _scoreManager.PlayerScores[1] / 100;
        _amountToLerp = Mathf.Abs(_currentValue - _newValue);
        _isLerping = true; 
    }
    private void Update()
    {
        if (_isLerping)
            LerpNewScore(_currentValue, _newValue);

        if(_currentLerpTime >= (_lerpTime * _amountToLerp))
            ResetLerp();
        
    }
    private void LerpNewScore(float from, float to)
    {
        if(_amountToLerp > 0.0f && _lerpTime > 0.0f)
        {
            _currentLerpTime += Time.deltaTime;
            if(_currentLerpTime > (_lerpTime * _amountToLerp))
            {
                float percentage = _currentLerpTime / (_lerpTime * _amountToLerp);
                float tmp = Mathf.Lerp(from, to, percentage);
                _scoreBar.material.SetFloat("_Score", tmp);
                
            }
        }
        else _isLerping = false;
    }
    private void ResetLerp()
    {
        _currentLerpTime = 0.0f;
    }
    private void FindScoreManager()
    {
        _scoreManager = GameManager.Instance.ScoreManager;
        _scoreManager.UpdatedScore += UpdateScoreBar;
    }

}
