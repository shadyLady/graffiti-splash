﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class InterfaceManager : MonoBehaviour
{
    [SerializeField] private GameObject _menu;
    [SerializeField] private GameObject _game;
    [SerializeField] private GameObject _result;
    [SerializeField] private GameObject _credits;

    [SerializeField] private GameObject _controls;
    [SerializeField] private GameObject _winImage;

    [SerializeField] private GameObject _selectOnStartMenu;
    [SerializeField] private GameObject _selectOnStartResult; 

    private void Start()
    {
        GameManager.Instance.OnGameStartEvent += HideMenu;
        GameManager.Instance.OnGameStartEvent += ShowGame;
        GameManager.Instance.OnLevelStart += HideMenu;
        GameManager.Instance.OnLevelStart += ShowGame;
        GameManager.Instance.OnReplayEnd += ShowResult;
        GameManager.Instance.OnReplayEnd += HideGame;
        GameManager.Instance.OnGameEndEvent += HideGame;
        GameManager.Instance.OnGameEndEvent += ShowResult;
        GameManager.Instance.OnLevelReload += HideResult;
        GameManager.Instance.OnLevelReload += ShowGame;
        GameManager.Instance.OnLevelStart += ShowControls;
        GameManager.Instance.OnGameEndEvent += HideControls;
        GameManager.Instance.OnMenuLoaded += HideResult;
        GameManager.Instance.OnMenuLoaded += ShowMenu;
        GameManager.Instance.OnLevelReload += ShowControls;
    }
    private void HideMenu()
    {
        _menu.SetActive(false);
    }
    private void ShowMenu()
    {
        _menu.SetActive(true);
        EventSystem.current.SetSelectedGameObject(_selectOnStartMenu);


    }
    private void HideGame()
    {
        _game.SetActive(false);
    }
    private void ShowGame()
    {
        _game.SetActive(true);
    }
    private void ShowResult()
    {
        _result.AddComponent<ResultWindow>();
        _result.GetComponent<ResultWindow>()._spriteRenderer = _winImage.GetComponent<Image>();
        _result.SetActive(true);

        EventSystem.current.SetSelectedGameObject(_selectOnStartResult);
        Debug.Log(EventSystem.current.currentSelectedGameObject);
    }
    private void HideResult()
    {
        _result.SetActive(false);
        Destroy(_result.GetComponent<ResultWindow>());
    }
    public void ShowCredits()
    {
        HideMenu();
        _credits.SetActive(true);
    }
    public void HideCredits()
    {
        _credits.SetActive(false);
        ShowMenu();
    }
    private void ShowControls()
    {
        _controls.SetActive(true);
    }
    private void HideControls()
    {
        _controls.SetActive(false);
    }
}
