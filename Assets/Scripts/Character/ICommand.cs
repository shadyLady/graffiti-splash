﻿using UnityEngine;
using System.Collections;

public interface ICommand 
{
    void Execute(Player player);
}

public class WalkCommand : MonoBehaviour, ICommand
{
    public void Execute(Player player)
    {
        player.SwitchState(new WalkingState(player));
    }
}

public class JumpCommand : MonoBehaviour, ICommand
{
    public void Execute(Player player)
    {
        player.SwitchState(new JumpingState(player));
    }
}