﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

[System.Serializable]
public struct InputData
{
    private GamePadState state;
    public GamePadState prevState; 

    public float Horizontal;
    public float Vertical;
    public bool Jump;
    public bool QuickAttack;
    public float ChargeAttack;

    public bool KeyboardCharge;
    public float KeyboardHorizontal;
    public float KeyboardVertical;
    public bool KeyboardJump;
    public bool KeyboardQuickAttack;
}
