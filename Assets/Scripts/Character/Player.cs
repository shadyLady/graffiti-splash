﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public enum PlayerState
{
    STATE_IDLE,
    STATE_WALKING,
    STATE_JUMPING,
    STATE_QUICKATTACK,
    STATE_JUMPATTACK,
    STATE_CHARGEATTACK,
    STATE_HIT
};

public class Player : Controller
{
    [SerializeField] private InputData _inputData;
    public InputData InputData
    {
        get { return _inputData; }
        set {
            Debug.Log("Player data updated " + _playerID);
            _inputData = value;
        }
    }

    private Controller _controller;
    public Controller Controller
    {
        get { return _controller; }
    }

    private Animator _animator;
    public Animator Animator
    {
        get { return _animator; }
    }

    [SerializeField]
    private Animator _animatorHead; 
    public Animator AnimatorHead
    {
        get { return _animatorHead; }
    }

    private int _playerID;
    public int PlayerID
    {
        get { return _playerID; }
        set { _playerID = value; }
    }

    private int _opponentID;
    public int OpponentID
    {
        get { return _opponentID; }
    }

    [SerializeField]
    private IPlayerState _state;
    public IPlayerState State
    {
        get { return _state; }
        set { _state = value; }
    }

    public PlayerState _playerState; 

    [SerializeField] private float _gravity;
    public float Gravity
    {
        get { return _gravity; }
    }

    [SerializeField] private float _movespeed;
    public float MoveSpeed
    {
        get { return _movespeed; }
    }

    [SerializeField] private float _jumpspeed;
    public float JumpSpeed
    {
        get { return _jumpspeed; }
    }

    [SerializeField]
    private int _facingDirection;
    public int FacingDirection
    {
        get { return _facingDirection; }
    }

    private bool _hasDoubleJumped = false; 
    public bool HasDoubleJumped
    {
        get { return _hasDoubleJumped; }
        set { _hasDoubleJumped = value; }
    }

    private bool _isOnSplash = false;
    public bool IsOnSplash
    {
        get { return _isOnSplash; }
    } 

    [SerializeField]
    private Vector3 _velocity; 
    public Vector3 Velocity
    {
        get { return _velocity; }
        set { _velocity = value; }
    }

    public GamePadState _prevState;

    private void Start()
    {
        _controller = GetComponent<Controller>();
        _inputData = new InputData();
        _animator = GetComponent<Animator>();
        SwitchState(new IdleState(this));

        if (_playerID == 0)
            _opponentID = 1;
        else _opponentID = 0;

        UpdateFacingDirection();

        _animator.SetTrigger("Spawn");
        _animatorHead.SetTrigger("Spawn");

    }
    public void GetInput()
    {
        if (GameManager.Instance.GameState != GameState.STATE_GAME)
            return; 

        GamePadState state = GamePad.GetState((PlayerIndex)_playerID);

        _inputData.Horizontal = state.ThumbSticks.Left.X;
        _inputData.Jump = _prevState.Buttons.A == ButtonState.Released && state.Buttons.A == ButtonState.Pressed;
        _inputData.QuickAttack = state.Buttons.B == ButtonState.Pressed;
        _inputData.ChargeAttack = state.Triggers.Right;
        _inputData.Vertical = state.ThumbSticks.Left.Y;

        _inputData.KeyboardCharge = Input.GetButton("P" + _playerID + "_KeyboardChargeAttack");
        _inputData.KeyboardHorizontal = Input.GetAxisRaw("P" + _playerID + "_Horizontal");
        _inputData.KeyboardJump = Input.GetButtonDown("P" + _playerID + "_Jump");
        _inputData.KeyboardQuickAttack = Input.GetButtonDown("P" + _playerID + "_QuickAttack");
        _prevState = state;
    }
    private void Update()
    {
        if (GameManager.Instance.GameState != GameState.STATE_GAME && GameManager.Instance.GameState != GameState.STATE_REPLAY)
            return;

        GetInput();
        _state.Update();
        UpdateFacingDirection();
    }
    private void UpdateFacingDirection()
    {
        int lastFacingDirection = _facingDirection;

        if (GameManager.Instance.Players[_opponentID].transform.position.x > transform.position.x)
            _facingDirection = 1;
        else _facingDirection = -1;

        if (_facingDirection != lastFacingDirection)
            UpdateSpriteOrientation();
    }
    private void UpdateSpriteOrientation()
    {
        Quaternion currentRotation = transform.localRotation;

        _animator.SetTrigger("Turn");
        _animatorHead.SetTrigger("Turn");

        if (_facingDirection == 1)
            currentRotation.y = 0;
        else if(_facingDirection == -1)
            currentRotation.y = -180;

        transform.localRotation = currentRotation;
    }
    public void SwitchState(IPlayerState state)
    {
        _state = state;
        _playerState = _state.GetState();
    }
    public void Move()
    {
        Move(_velocity);
    }
}


public interface IPlayerState
{
    void Update();
    PlayerState GetState();
    void SwitchState(IPlayerState state);
}

public class IdleState : IPlayerState
{
    private Player _player;
    private Vector3 _velocity;

    //Debugging purposes
    private PlayerState _state = PlayerState.STATE_IDLE;

    public IdleState(Player player)
    {
        _player = player;
    }
    public void Update()
    {
        if (_player.InputData.Horizontal != 0 || _player.InputData.KeyboardHorizontal != 0)
            SwitchState(new WalkingState(_player));
        else if (_player.InputData.Jump)
            SwitchState(new JumpingState(_player));
        else if (_player.InputData.KeyboardJump)
            SwitchState(new JumpingState(_player));
        else if (_player.InputData.QuickAttack)
            SwitchState(new QuickAttackState(_player));
        else if (_player.InputData.KeyboardQuickAttack)
            SwitchState(new QuickAttackState(_player));
        else if (_player.InputData.ChargeAttack > 0.1f || _player.InputData.KeyboardCharge)
            SwitchState(new ChargeAttackState(_player));
        else if (_player.InputData.Vertical < -0.2f)
        {
            if (_player.CollisionData.StandingOnPlatform)
            {
                _player.CanFallThrough = true;
                Move();
            }
        }

        Move();
    }
    private void Move()
    {
        _velocity.y += _player.Gravity;

        if (_player.CollisionData.BottomCollision || _player.CollisionData.TopCollision)
            _velocity.y = 0;

        _player.Velocity = _velocity;
        _player.Move();
    }
    public PlayerState GetState()
    {
        return _state;
    }
    public void SwitchState(IPlayerState state)
    {
        _player.SwitchState(state);
    }
}

public class WalkingState : IPlayerState
{
    private Player _player;
    private Vector3 _velocity;
    private int _curDir;
    private bool _turning = false;

    private PlayerState _state = PlayerState.STATE_WALKING; 

    public WalkingState(Player player)
    {
        _player = player;
        _player.Velocity = _velocity;


        float input = 0;

        if (_player.InputData.Horizontal != 0) input = _player.InputData.Horizontal;
        else if (_player.InputData.KeyboardHorizontal != 0) input = _player.InputData.KeyboardHorizontal;

        _player.Animator.SetBool("Backwards", IsWalkingBackwards(Mathf.Sign(input)));
        _player.Animator.SetBool("Walking", true);
        _player.AnimatorHead.SetBool("Backwards", IsWalkingBackwards(Mathf.Sign(input)));
        _player.AnimatorHead.SetBool("Walking", true);

        _curDir = _player.FacingDirection;
    }
    public void Update()
    {
        if (_player.InputData.Horizontal == 0 && _player.InputData.KeyboardHorizontal == 0)
            SwitchState(new IdleState(_player));
        else if (_player.InputData.Jump)
        {
            SwitchState(new JumpingState(_player));
        }
        else if (_player.InputData.KeyboardJump)
            SwitchState(new JumpingState(_player));
        else if (_player.InputData.QuickAttack)
            SwitchState(new QuickAttackState(_player));
        else if (_player.InputData.KeyboardQuickAttack)
            SwitchState(new QuickAttackState(_player));

        float input = 0;

        if (_player.InputData.Horizontal != 0) input = _player.InputData.Horizontal;
        else if (_player.InputData.KeyboardHorizontal != 0) input = _player.InputData.KeyboardHorizontal;

        Move(input);
    }
    private void Move(float input)
    {
        float moveDir = Mathf.Sign(_velocity.x);
        CheckFacingDirection();

        if (IsWalkingBackwards(Mathf.Sign(moveDir)))
            _velocity.x = input * (_player.MoveSpeed * 0.7f);
        else _velocity.x = input * _player.MoveSpeed;



        _velocity.y += _player.Gravity;

        if (_player.CollisionData.BottomCollision || _player.CollisionData.TopCollision)
            _velocity.y = 0;

        _player.Velocity = _velocity;
        _player.Move();
    }
    private void CheckFacingDirection()
    {
        if(_player.FacingDirection != _curDir)
        {
            _turning = true;
            _player.Animator.SetBool("Walking", false);
            _player.AnimatorHead.SetBool("Walking", false);
            _player.Animator.SetBool("Backwards", IsWalkingBackwards(Mathf.Sign(_velocity.x)));
            _player.AnimatorHead.SetBool("Backwards", IsWalkingBackwards(Mathf.Sign(_velocity.x)));
            _player.AnimatorHead.SetBool("Walking", true);
            _player.Animator.SetBool("Walking", true);
            _curDir = _player.FacingDirection;
            _turning = false;
        }
    }
    private bool IsWalkingBackwards(float dir)
    {
        if (dir != _player.FacingDirection)
        {
            return true;
        }
        else return false;
    }
    public void SwitchState(IPlayerState state)
    {
        _player.Animator.SetBool("Walking", false);
        _player.AnimatorHead.SetBool("Walking", false);
        _player.SwitchState(state);
    }
    public PlayerState GetState()
    {
        return _state;
    }
}

public class JumpingState : IPlayerState
{
    private Player _player;
    private Vector3 _velocity;
    private Vector3 _lastVelocity;
    private bool _falling = false;
    private PlayerState _state = PlayerState.STATE_JUMPING; 

    public JumpingState(Player player)
    {
        _player = player;
        _velocity = _player.Velocity;

        Move();
        Jump();
    }
    public void Update()
    {
        if (_player.InputData.QuickAttack)
        {
            _player.Animator.SetBool("Jump", false);
            _player.AnimatorHead.SetBool("Jump", false);
            _player.Animator.SetBool("Fall", true);
            _player.AnimatorHead.SetBool("Fall", true);

            SwitchState(new JumpAttackState(_player));
        }

        if(_player.InputData.KeyboardQuickAttack)
        {
            _player.Animator.SetBool("Jump", false);
            _player.AnimatorHead.SetBool("Jump", false);
            _player.Animator.SetBool("Fall", true);
            _player.AnimatorHead.SetBool("Fall", true);

            SwitchState(new JumpAttackState(_player));
        }


        if (_player.InputData.Jump)
        {
            _player.Animator.SetBool("Fall", false);
            _player.AnimatorHead.SetBool("Fall", false);
            _falling = false;
            _player.HasDoubleJumped = true;
            Jump();
        }

        if(_player.InputData.KeyboardJump)
        {
            _player.Animator.SetBool("Fall", false);
            _player.AnimatorHead.SetBool("Fall", false);
            _falling = false;
            _player.HasDoubleJumped = true;
            Jump();
        }

        Move();

        if (_velocity.y < 0.0f && !_falling)
        {
            Debug.Log("Falling");
            _falling = true;
            Debug.Log(_falling);
            _player.Animator.SetBool("Jump", false);
            Debug.Log(_player.Animator.GetBool("Jump"));
            _player.AnimatorHead.SetBool("Jump", false);
            _player.Animator.SetBool("Fall", true);
            _player.AnimatorHead.SetBool("Fall", true);
        }

        if (_player.CollisionData.BottomCollision)
        {
            _player.Animator.SetBool("Fall", false);
            _player.AnimatorHead.SetBool("Fall", false);
            _player.HasDoubleJumped = false;
            SwitchState(new IdleState(_player));
        }
    }
    private void Jump()
    {
        _player.Animator.SetBool("Jump", true);
        _player.AnimatorHead.SetBool("Jump", true);
        _velocity.y = _player.JumpSpeed;
    }
    private void Move()
    {
        _lastVelocity = _velocity;


        float input = 0;

        if (_player.InputData.Horizontal != 0) input = _player.InputData.Horizontal;
        else if (_player.InputData.KeyboardHorizontal != 0) input = _player.InputData.KeyboardHorizontal;

        _velocity.x = input * _player.MoveSpeed;
        _velocity.y += _player.Gravity;

        if (_player.CollisionData.TopCollision)
            _velocity.y = 0;

        _player.Velocity = _velocity;
        _player.Move();
    }
    public void SwitchState(IPlayerState state)
    {

        _player.SwitchState(state);
    }
    public PlayerState GetState()
    {
        return _state;
    }
}

public class QuickAttackState : IPlayerState
{
    private Player _player;
    private float _attackRange = 2;
    private Vector3 _velocity;
    private PlayerState _state = PlayerState.STATE_QUICKATTACK;

    public QuickAttackState(Player player)
    {
        _player = player;
        _velocity = Vector3.zero;
        _player.Animator.SetBool("Fall", false);
        _player.AnimatorHead.SetBool("Fall", false);
        _player.AnimatorHead.SetTrigger("QuickAttack");

        Attack();
    }
    public void Update()
    {
        Move();

        if(!_player.InputData.QuickAttack)
            SwitchState(new IdleState(_player));
    }
    private void Attack()
    {
        Vector3 origin = _player.transform.position + (new Vector3(((_player.GetComponent<SpriteRenderer>().bounds.size.x * 0.5f) * _player.FacingDirection) + 0.1f, 0.05f));
        RaycastHit2D hit = Physics2D.Raycast(origin, (Vector2.up * _player.FacingDirection), _attackRange);
        Debug.DrawRay(origin, Vector2.right * _player.FacingDirection, Color.red);

        if (hit)
        {
            if (hit.collider.tag == "Player")
            {
                if (hit.collider != _player.GetComponent<Collider2D>())
                {
                    GameManager.Instance.SplashManager.ActivateSplash(_player.OpponentID, "Regular", hit.point, _player.FacingDirection, 1.0f);
                    GameManager.Instance.Players[_player.OpponentID].State.SwitchState(new HitState(GameManager.Instance.Players[_player.OpponentID]));
                }
            }
        }
    }
    private void Move()
    {
        _velocity.y += _player.Gravity;
        _player.Velocity = _velocity;
        _player.Move();
    }
    public void SwitchState(IPlayerState state)
    {
        _player.SwitchState(state);
    }
    public PlayerState GetState()
    {
        return _state;
    }
}
public class JumpAttackState : IPlayerState
{
    private Player _player;
    private float _attackRange = 2.0f;
    private PlayerState _state = PlayerState.STATE_JUMPING;
    private Vector3 _velocity;

    public JumpAttackState(Player player)
    {
        _player = player;
        _player.AnimatorHead.SetTrigger("QuickAttack");
        _velocity = _player.Velocity;
        Attack();
    }
    public void Update()
    {
        Move();

        if(!_player.InputData.QuickAttack && !_player.InputData.KeyboardQuickAttack)
            SwitchState(new IdleState(_player));
        if (_player.CollisionData.BottomCollision)
            SwitchState(new IdleState(_player));
    }
    private void Attack()
    {
        Vector3 origin = _player.transform.position + (new Vector3((_player.GetComponent<SpriteRenderer>().bounds.size.x * 0.5f) + 0.01f * _player.FacingDirection, 0.5f));
        RaycastHit2D hit = Physics2D.Raycast(origin, Vector2.right * _player.FacingDirection, _attackRange);

        if (hit)
        {
            if (hit.collider.tag == "Player")
            {
                if (hit.collider != _player.GetComponent<Collider2D>())
                {
                    GameManager.Instance.SplashManager.ActivateSplash(_player.OpponentID, "Regular", hit.point, _player.FacingDirection, 1.0f);
                    GameManager.Instance.Players[_player.OpponentID].State.SwitchState(new HitState(GameManager.Instance.Players[_player.OpponentID]));
                }
            }
        }
    }
    public void SwitchState(IPlayerState state)
    {
        _player.SwitchState(state);
    }
    public PlayerState GetState()
    {
        return _state;
    }
    private void Move()
    {
        _velocity.y += _player.Gravity;
        _player.Velocity = _velocity;
        _player.Move();
    }
}
public class ChargeAttackState : IPlayerState
{
    private Player _player;
    private float _chargeDuration;
    private float _attackRange = 4;
    private PlayerState _state = PlayerState.STATE_CHARGEATTACK; 

    public ChargeAttackState(Player player)
    {
        _player = player;
        
        GameManager.Instance.ChargeBars[_player.PlayerID].GetComponent<Animator>().SetBool("Charging", true);
        _player.Animator.SetBool("Charging", true);
        _player.AnimatorHead.SetBool("Charging", true);
    }
    public void Update()
    {
        _chargeDuration += Time.deltaTime;

        if (_player.InputData.Horizontal != 0)
            SwitchState(new IdleState(_player));
        else if (_player.InputData.Jump)
            SwitchState(new JumpingState(_player));

        if(_player.InputData.ChargeAttack < 0.1f && !_player.InputData.KeyboardCharge)
        {
            _player.Animator.SetBool("Charging", false);
            _player.AnimatorHead.SetBool("Charging", false);
            CalculateAttackSeverity();
        }

        if(_chargeDuration >= 4.0f)
        {
            GameManager.Instance.ChargeBars[_player.PlayerID].GetComponent<Animator>().SetBool("Charging", false);
        }

        _player.Move();
    }
    private void CalculateAttackSeverity()
    {
        if (_chargeDuration >= 0.0f && _chargeDuration < 0.25f)
            Attack(1);
        else if (_chargeDuration >= 0.25f && _chargeDuration < 0.5f)
            Attack(1.5f);
        else if (_chargeDuration >= 0.5f && _chargeDuration < 0.75f)
            Attack(2.5f);
        else if (_chargeDuration >= 0.75f && _chargeDuration < 1.0f)
            Attack(3.5f);
        else Attack(4.5f);

        GameManager.Instance.ChargeBars[_player.PlayerID].GetComponent<Animator>().SetBool("Charged", false);
    }
    private void Attack(float intensity)
    {
        GameManager.Instance.ChargeBars[_player.PlayerID].GetComponent<Animator>().SetBool("Charging", false);

        _player.Animator.SetBool("Charging", false);
        _player.AnimatorHead.SetBool("Charging", false);

        Vector3 origin = _player.transform.position + (new Vector3(((_player.GetComponent<Collider2D>().bounds.size.x * 0.5f) + 0.0001f) * _player.FacingDirection, 0.5f, 0.0f));
        RaycastHit2D hit = Physics2D.Raycast(origin, Vector2.right * _player.FacingDirection, _attackRange);

        if(hit)
        {
            if(hit.collider.tag == "Player")
            {
                if (hit.collider != _player.GetComponent<Collider2D>())
                {
                    GameManager.Instance.SplashManager.ActivateSplash(_player.OpponentID, "Large", hit.point, _player.FacingDirection, intensity);
                    GameManager.Instance.Players[_player.OpponentID].SwitchState(new HitState(GameManager.Instance.Players[_player.OpponentID]));
                }
            }
        }

        SwitchState(new IdleState(_player));
    }
    public void SwitchState(IPlayerState state)
    {
        GameManager.Instance.ChargeBars[_player.PlayerID].GetComponent<Animator>().SetBool("Charging", false);

        _player.Animator.SetBool("Charging", false);
        _player.AnimatorHead.SetBool("Charging", false);

        _player.SwitchState(state);
    }
    public PlayerState GetState()
    {
        return _state; 
    }
}

public class HitState : IPlayerState
{
    private Player _player;
    private float _staggerTime = 0.001f;
    private PlayerState _state = PlayerState.STATE_HIT; 

    public HitState(Player player)
    {
        _player = player;
        _player.Animator.SetTrigger("Hit");
        _player.AnimatorHead.SetTrigger("Hit");
        GameManager.Instance.CameraManager.ShakeItBaby(0.6f, 1.0f);
        GameManager.Instance.AudioManager.PlayClip(Random.Range(4, 6));
    }
    public void Update()
    {
        if (_staggerTime > 0)
            _staggerTime -= Time.deltaTime;
        else
            _player.SwitchState(new IdleState(_player));

        _player.Move();
    }
    public void SwitchState(IPlayerState state)
    {
        _player.SwitchState(state);
    }
    public PlayerState GetState()
    {
        return _state;
    }
}