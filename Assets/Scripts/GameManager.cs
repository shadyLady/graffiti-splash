﻿using UnityEngine;
using System.Collections;

public delegate void VoidDelegate();

public enum GameState
{
    STATE_MENU, 
    STATE_GAME,
    STATE_RESULT,
    STATE_REPLAY
};

public class GameManager : Singleton<GameManager>
{
    private PlayerData[] _playerData; 
    public PlayerData[] PlayerData
    {
        get { return _playerData; }
    }

    [SerializeField]
    private Player[] _players;
    public Player[] Players
    {
        get { return _players; }
    }

    [SerializeField]
    private Vector3[] _spawnPositions; 

    private SplashManager _splashManager; 
    public SplashManager SplashManager
    {
        get { return _splashManager; }
    }

    private ScoreManager _scoreManager;
    public ScoreManager ScoreManager
    {
        get { return _scoreManager; }
    }

    private CameraManager _cameraManager; 
    public CameraManager CameraManager
    {
        get { return _cameraManager; }
    }

    [SerializeField] private GameState _gameState; 
    public GameState GameState
    {
        get { return _gameState; }
    }

    private event VoidDelegate _onGameStartEvent;
    public VoidDelegate OnGameStartEvent
    {
        get { return _onGameStartEvent; }
        set { _onGameStartEvent = value; }
    }

    private event VoidDelegate _onGameEndEvent; 
    public VoidDelegate OnGameEndEvent
    {
        get { return _onGameEndEvent; }
        set { _onGameEndEvent = value; }
    }

    private event VoidDelegate _onLevelReload;
    public VoidDelegate OnLevelReload
    {
        get { return _onLevelReload; }
        set { _onLevelReload = value; }
    }

    private event VoidDelegate _onLevelStart;
    public VoidDelegate OnLevelStart
    {
        get { return _onLevelStart; }
        set { _onLevelStart = value; }
    }

    private event VoidDelegate _onReplayStart; 
    public VoidDelegate OnReplayStart
    {
        get { return _onReplayStart; }
        set { _onReplayStart = value; }
    }

    private event VoidDelegate _onReplayEnd; 
    public VoidDelegate OnReplayEnd
    {
        get { return _onReplayEnd; }
        set { _onReplayStart = value; }
    }

    private event VoidDelegate _onMenuLoaded;
    public VoidDelegate OnMenuLoaded
    {
        get { return _onMenuLoaded; }
        set { _onMenuLoaded = value; }
    }

    private AudioManager _audioManager; 
    public AudioManager AudioManager
    {
        get { return _audioManager; }
    }

    [SerializeField]
    private GameObject[] _chargeBars; 
    public GameObject[] ChargeBars
    {
        get { return _chargeBars; }
    }

    [SerializeField]
    private LevelCountdown _levelCountDown;
    private LightController _light;
    private float _idleTime = 0.0f;
    [SerializeField] private float _timeBeforeReplay = 0.0f;
    [SerializeField]
    private bool _canDoReplay = false; 

    protected override void Awake()
    {
        base.Awake();

        Application.targetFrameRate = 60; 

        _light = FindObjectOfType<LightController>();
        _audioManager = FindObjectOfType<AudioManager>();

        _gameState = GameState.STATE_MENU;
    }
    private void Update()
    {
        if (!_canDoReplay)
            return; 

        if(_gameState != GameState.STATE_REPLAY)
        {
            if (_idleTime < _timeBeforeReplay)
                _idleTime += Time.deltaTime;
            else
            {
                StartReplay();
            }
        }

    }
    public void StartGame()
    {
        if (_gameState != GameState.STATE_MENU)
            return;

        StartCoroutine(_light.SwitchOn(1.5f, 5));
        _audioManager.StartSwitch(1);

        if (_onGameStartEvent != null)
            _onGameStartEvent();

        ReadyLevel();
        StartCoroutine(_levelCountDown.StartCountDown(1.0f, 2, StartLevel));
    }
    private void StartLevel()
    {
        _gameState = GameState.STATE_GAME;

        if (_onLevelStart != null)
            _onLevelStart();
    }
    private void ReadyLevel()
    {
        InitializePlayers();
        CreatePlayers();

        _cameraManager = FindObjectOfType<CameraManager>();
        _splashManager = new SplashManager();
        _scoreManager = new ScoreManager();
    }
    private void InitializePlayers()
    {
        _playerData = new PlayerData[2];

        _playerData[0] = new PlayerData(0, Resources.Load<Player>("Prefabs/Player/Metal Head/MetalHead_Green"), new Color(205f/255f, 150f/255f, 0f/255f));
        _playerData[1] = new PlayerData(1, Resources.Load<Player>("Prefabs/Player/Metal Head/MetalHead_Pink"), new Color(153f/255f, 63f/255f, 255f/255f));
    }
    private void CreatePlayers()
    {
        _players = new Player[_playerData.Length];

        for(int i = 0; i < _players.Length; i++)
        {
            _players[i] = Instantiate(_playerData[i].Prefab, _spawnPositions[i], Quaternion.identity) as Player;
            _players[i].PlayerID = _playerData[i].PlayerID;
            _players[i].GetComponent<SpriteRenderer>().sortingOrder = (_playerData[i].PlayerID + 1)* 10;
        }
    }

    public void StartReplay()
    {
        if (_gameState != GameState.STATE_MENU)
            return;

        ReadyLevel();

        if (_onReplayStart != null)
            _onReplayStart();

        _gameState = GameState.STATE_REPLAY;
        StartCoroutine(_levelCountDown.StartCountDown(1.0f, 2, null));

        if (_onLevelStart != null)
            _onLevelStart();
    }

    public void StopReplay()
    {
        if (_gameState != GameState.STATE_REPLAY)
            return;

        _idleTime = 0.0f; 

        if (_onReplayEnd != null)
            _onReplayEnd();

        ReturnToMenu();
    }

    public void EndLevel()
    {
        if (_gameState != GameState.STATE_GAME)
            return;

        _audioManager.StartSwitch(0);

        _gameState = GameState.STATE_RESULT;

        if (_onGameEndEvent != null)
            _onGameEndEvent();
    }
    public void Rematch()
    {
        if (_gameState != GameState.STATE_RESULT)
            return;

        if (_onLevelReload != null)
            _onLevelReload();

        _audioManager.StartSwitch(0);

        for (int i = 0; i < _players.Length; i++)
        {
            Destroy(_players[i].gameObject);
        }

        CreatePlayers();

        if (_onGameStartEvent != null)
            _onGameStartEvent();

        _gameState = GameState.STATE_GAME;
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    public void ReturnToMenu()
    {
        if (_gameState != GameState.STATE_RESULT && _gameState != GameState.STATE_REPLAY)
            return;

        _audioManager.StartSwitch(1);

        if (_onMenuLoaded != null)
            _onMenuLoaded();

        for (int i = 0; i < _players.Length; i++)
        {
            Destroy(_players[i].gameObject);
        }

        _gameState = GameState.STATE_MENU;
    }
}
